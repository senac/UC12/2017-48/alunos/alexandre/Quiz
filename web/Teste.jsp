<%@page import="br.com.senac.quiz.model.Usuario"%>
<%@page import="java.util.List"%>
<%@page import="br.com.senac.quiz.dao.UsuarioDAO"%>
<html>
    <body>

        <%
            String nome = request.getParameter("nome");
            UsuarioDAO dao = new UsuarioDAO();
            List<Usuario> lista = dao.getLista();
        %>

        <% out.print(nome); %>
        <form>
            Nome: <input type="text" name="nome" /> 
            <input type="submit" value="Pesquisar" />
        </form>


        <table border="1">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Nome</th>
                    <th>Apelido</th>
                </tr>
            </thead>
            <tbody>

                <% for (Usuario u : lista) { %>

                <tr>
                    <td><% out.print(u.getId()); %></td>
                    <td><% out.print(u.getNome()); %></td>
                    <td><% out.print(u.getApelido()); %></td>
                </tr>

                <% }%>

        </table>

    </body>
</html>
