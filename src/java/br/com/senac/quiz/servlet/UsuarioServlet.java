package br.com.senac.quiz.servlet;

import br.com.senac.quiz.dao.UsuarioDAO;
import br.com.senac.quiz.model.Usuario;

import java.io.PrintWriter;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class UsuarioServlet extends HttpServlet{
   private Usuario usuario;
   private UsuarioDAO dao;
   
   
    @Override
    protected  void doPost(HttpServletRequest requisicao, HttpServletResponse resposta) throws ServletException, IOException {
        
        PrintWriter saida = resposta.getWriter();
        try {

            String nome = requisicao.getParameter("nome");
            String senha = requisicao.getParameter("senha");
            String apelido = requisicao.getParameter("apelido");

            usuario = new Usuario();
            usuario.setNome(nome);
            usuario.setSenha(senha);
            usuario.setApelido(apelido);

            dao = new UsuarioDAO();
            dao.inserir(usuario);

            saida.println("<html>");
            saida.println("<body>");
            saida.println("Salvo com sucesso!");
            saida.println("</body>");
            saida.println("</html>");

        } catch (Exception ex) {
            saida.println("<html>");
            saida.println("<body>");
            saida.println("Salvo com sucesso!");
            saida.println();
            saida.println("</body>");
            saida.println("</html>");


            }
 
        }
    }