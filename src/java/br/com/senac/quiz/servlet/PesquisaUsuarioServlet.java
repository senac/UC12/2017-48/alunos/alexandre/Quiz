package br.com.senac.quiz.servlet;

import br.com.senac.quiz.dao.UsuarioDAO;
import br.com.senac.quiz.model.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class PesquisaUsuarioServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.pesquisar(req, resp); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.pesquisar(req, resp); //To change body of generated methods, choose Tools | Templates.
    }

    private void pesquisar(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        UsuarioDAO dao = new UsuarioDAO();

        List<Usuario> lista = dao.getLista();

        PrintWriter saida = resp.getWriter();

        saida.print("<html>");
        saida.print("<body>");

        saida.print("<table>");

        saida.print("<tr><td>ID</td><td>Nome</td><td>Apelido</td></tr>");
        for (Usuario u : lista) {

            saida.print(
                      "<tr>"
                    + "<td>" + u.getId() +      "</td>"
                    + "<td>" + u.getNome() +    "</td>"
                    + "<td>" + u.getApelido() + "</td>"
                    + "</tr>");

        }

        saida.print("</table>");

        saida.print("</body>");
        saida.print("</html>");

    }

}
